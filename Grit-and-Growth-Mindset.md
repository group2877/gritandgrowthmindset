# **Grit and Growth Mindset**

# I. Grit

## **1. Paraphrase (summarize) the video in a few lines. Use your own words.**

In the video, the speaker says that IQ is not the only difference between her best and worst students. She was firmly convinced that every one of her students could learn the material if they could work long and hard enough. From her years of teaching, she came to the conclusion that what's missing in education is a much better understanding of students and learning from a motivational and psychological perspective. She further became a psychologist and started studying kids and adults in very different contexts. She found that one characteristic emerged as a significant predictor of success and that was grit.

Grit is passion and perseverance for very long-term goals. Grit is dreaming for the future and working hard on a long-term basis to make that future a reality. Talent doesn't make one gritty. In the speaker's data, grit is usually unrelated or even inversely related to measures of talent. The best way to build grit is the belief called **Growth Mindset**.

A Growth Mindset is a belief that the ability to learn is not fixed and can change with one's effort. According to the speaker, when kids read and learn about the brain and how it changes and grows in response to challenges, they're much more likely to persevere when they fail, because they don't believe failure is a permanent condition. So, a growth mindset is a great idea for building grit.

## **2. What are your key takeaways from the video to take action on?**

Following are my takeaways from the video:

- I should have the grit to push myself and achieve my dreams.
- Depending on talents alone won't get me anywhere.
- Realising that failure is not a permanent condition is crucial for achieving long-term goals.

## II. Introduction to Growth Mindset

## **3. Paraphrase (summarize) the video in a few lines in your own words.**

The video essentially talks about two types of mindsets: **Fixed Mindsets** and **Growth Mindsets**

People who have a fixed mindset have the following characteristics:

- They believe that skills and intelligence are set and you either have them or don't have them.
- They believe that some people are naturally good at things while others are not.
- They believe that you **are not** in control of your abilities.

People who have a growth mindset have the following characteristics:

- They believe that skills and intelligence are grown and developed.
- They believe that people who are good at something are good because they built that ability. The people who aren't, are not good because they haven't done the work.
- They believe that you **are** in control of your abilities.

People who have a growth mindset tend to learn, grow and achieve more over time than people with a fixed mindset. A growth mindset is a foundation for learning.

## **4.What are your key takeaways from the video to take action on?**

Following are my takeaways from the video:

- To keep on learning with grit, I should have a growth mindset.
- I should learn to believe that skills are built and a person is not born with them.

# III.Understanding Internal Locus of Control

## **5.What is the Internal Locus of Control? What is the key point in the video?**

Locus of control is a concept that is essentially the degree to which you believe you have control over your life. There are two types of locus of control:
**External Locus of Control** and **Internal Locus of Control**

A person has an internal locus of control when they believe that the factors they control lead to their outcomes unlike a person with an external locus of control who believe that external factors affect their outcome.

The key point in this video is that Internal Locus of Control is the key to staying motivated. You must feel that you are controlling your life and you are responsible for your outcomes to feel motivated all of the time. To have an internal locus of control, simply solve problems in your own life and take some time to appreciate yourself for the actions that you performed to solve this problem.

# IV. How to build a growth mindset?

## **6.Paraphrase (summarize) the video in a few lines in your own words.**

The video essentially talks about how to build a growth mindset. The following steps are the keys to building a growth mindset:

- Believe in your ability to figure things out
- Question your assumptions
- Develop your life curriculum
- Honor the struggle during failure and never give up

## **7.What are your key takeaways from the video to take action on?**

Following are my key takeaways from the video:

- I should never quit when stuck somewhere and always believe in my ability to solve problems.
- I should always question my assumptions and never go forward with just assumptions and should verify them.
- I should set a life curriculum to achieve my future goals to start working toward it

## **8.What are one or more points that you want to take action on from the manual? (Maximum 3)**

- I should refer to the documentation, google, stack overflow, GitHub issues, and the internet before I seek help from my colleagues as mentioned in point 8 in the manual.
- I should realise that confusion means there is something more to understand as mentioned in point 10.
- I should focus on achieving mastery so that I may be able to solve any problem given to me quickly and efficiently, anywhere anytime as mentioned in point 14.
